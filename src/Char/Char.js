import React, { Component } from 'react';


const char = (props) => {
    const style = {
        display: 'inline-block',
        padding: '16px',
        textAligh: 'center',
        margin: '16px',
        border: '1px solid blue'
    }
    return(
        <div style={style} onClick={props.deleteText}>
            {props.text}
        </div>
    )
}
export default char;


