import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

const app = props => {  
  const [personsState, setPersonsState] = useState({
    persons: [
      { name: 'Hedyat', age: 40 },
      { name: 'Aaliyah', age: 7 },
      { name: 'Rayyan', age: 5 }
    ],
    
  })
  const [otherState, setOtherState] = useState ({
    otherState: 'some other value'
  })

  console.log(personsState,otherState);
  
  const switchNameHandler = () => {
    // console.log('Was clicked!');
    // DON'T DO THIS: this.state.persons[0].name = 'Maximilian';
    setPersonsState({
      persons: [
        { name: 'Hedyat Ullah', age: 38 },
        { name: 'Aaliyah', age: 6 },
        { name: 'Rayyan', age: 4 }
      ]
    });
  };

    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p>This is really working!</p>
        <button onClick={switchNameHandler}>Switch Name</button>
        <Person
          name={personsState.persons[0].name}
          age={personsState.persons[0].age}
        />
        <Person
          name={personsState.persons[1].name}
          age={personsState.persons[1].age}
          click={switchNameHandler}
        >
          My Hobbies: Racing
        </Person>
        <Person
          name={personsState.persons[2].name}
          age={personsState.persons[2].age}
        />
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Does this work now?'));  
}

export default app;



