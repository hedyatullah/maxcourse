import React, { Component } from 'react';
import Radium, { StyleRoot } from 'radium'
import './App.css';
import Person from './Person/Person1';

class App extends Component {
  state = {
    persons: [
      {id: 1, firstname:'Hedyat', lastname:'Ullah'},
      {id: 2, firstname:'Aaliyah', lastname:'Hedayat'},
      {id: 3, firstname:'Rayyan', lastname:'Abdullah'}    
    ],
    showlist: true
  }
  toggleList = () => {    
    const showperson = this.state.showlist;
    this.setState({showlist: !showperson})
  }
  deletePerson = (index) => {
    const persons = [...this.state.persons];
    persons.splice(index,1);
    this.setState({persons: persons})
  }
  changePersonName = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    })
    const person = {
      ...this.state.persons[personIndex]
    }
    person.firstname = event.target.value;

    const persons = [...this.state.persons];

    persons[personIndex] = person;

    this.setState({persons: persons})
  }
  render(){

      const style = {     // this is inline styling, here the limitation is we can't use sudo sellector
        backgroundColor: 'green',
        color: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '8px',
        cursor: 'pointer',
        ':hover': {
          backgroundColor: 'lightgreen',
          color: 'black'
        }
      }

    let persons = null;

    if(this.state.showlist === true){
      persons = (
        <div>
        {
          this.state.persons.map((person, index) => {
            return <Person 
              click={() => this.deletePerson(index)}
              fname={person.firstname}
              lname={person.lastname}
              key={person.id}
              changed={(event) => this.changePersonName(event,person.id)}
            />
          })
        }
      </div>  
      )
      style.backgroundColor = 'red';  
      style[':hover'] = {
        backgroundColor: 'salmon',
        color: 'black'
      }    
    }

    const classes = [];

    if(this.state.persons.length <=2 ){
      classes.push('red')
    }
    if(this.state.persons.length <=1 ){
      classes.push('bold')
    }


    return(      
      <StyleRoot>
        <div className="App">
        <p className={classes.join(' ')}>Welcome to React Program </p>       
        <p><button 
            style={style} 
            onClick={this.toggleList}>Toggle Person</button></p> 
        {persons}
      </div>   
      </StyleRoot>  
    )
  }
}
export default Radium(App);