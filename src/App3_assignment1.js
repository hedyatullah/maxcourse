import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';

class App extends Component {  
  state ={
    userInput: ''
  }
  textHandler = (event) => {
    this.setState({userInput:event.target.value})
  }
  deleteChar = (index) => {
    const text = this.state.userInput.split('');
    text.splice(index,1);
    const updatedText = text.join('');
    this.setState({userInput: updatedText})
  }
  

  render(){
    const textToChar = this.state.userInput.split('').map((ch, index) => {
      return (
        <Char 
          text={ch}
          deleteText={() => this.deleteChar(index)}
        />
      )
    })
    return(
      <div>
        <p>Welcome to the world of Text...</p>
        <p>Input Text in Box</p>
        <p><input type="text" onChange={this.textHandler} value={this.state.userInput} /></p>
        <p>{this.state.userInput}</p>
        <p>Lenght of the text: {this.state.userInput.length}</p>
        <Validation length={this.state.userInput.length} />
        {textToChar}
      </div>
    )
  }
}

export default App;



