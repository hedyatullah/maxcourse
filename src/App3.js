import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';
import Person from './Person/Person';

class App extends Component {  
  state = {
    persons: [
      {id: 1, firstname:'Hedyat', lastname:'Ullah'},
      {id: 2, firstname:'Aaliyah', lastname:'Hedayat'},
      {id: 3, firstname:'Rayyan', lastname:'Ullah'}
    ],
    showList: false
  }
  showPersons = () => {
    console.log('showperson clicked..')
    const doesShow = this.state.showList;
    this.setState({showList: !doesShow})
  }
  nameChangedHandler = (event,id) => {
    //console.log(index)
    //console.log(event)
    //const person = this.state.persons.findIndex(p => {
    //  return p.index
    //})
    //const newPerosns = this.state.persons;
    //newPerosns[index] = event.target.value;
    
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })
    const person = {
      ...this.state.persons[personIndex]
    };
    person.firstname = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({persons:persons})
  }
  deletePersonHandler = (index) => {
    //const persons = this.state.persons;    // not a ideal way as its not copy the object just copy a pointer, so it directly mutate the data that might get issue further so goo way is to copy a object
    //const persons = this.state.persons.slice();  // this is alternate way to do the same. it copy the entire object and save in the variable
    //const person = Object.assign({},this.state.persons[])  this is also an another way to copy the object

    const persons = [...this.state.persons];  // this is modern way and its copy the object.

    persons.splice(index,1)
    this.setState({persons: persons})
  }

  render(){
    let persons = null;

    if(this.state.showList) {
      persons = (
        <div>
        {
          this.state.persons.map((person, index) => {            
              return <Person
                        click={() => this.deletePersonHandler(index)} 
                        name={person.firstname}
                        age={person.lastname}
                        key={person.id}
                        changed={(event)  => this.nameChangedHandler(event,person.id)}
                      />
          })
        }
      </div>
      )
    }

    return(
      <div className="App">
        <p>React is working..</p>
        <p><button onClick={this.showPersons}>Toggle List</button></p>
        {persons}
      </div>
    )
  }
}
export default App;



